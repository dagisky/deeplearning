from __future__ import division, print_function, absolute_import

import tensorflow as tf

from tensorflow.examples.tutorials.mnist import input_data

mnist = input_data.read_data_sets("/tmp/data", one_hot=True)

learning_rate = 0.001
num_steps = 2000
batch_size = 128

num_input = 784 # MNIST data input (img shape: 28*28)
num_classes = 10 # MNIST total classes (0-9 digits)
dropout = 0.25 # Dropout, probability to drop a unit

def ann(xIn, n_hidden = num_classes):
    # Define a scope for reusing the variables
    weights = tf.Variable(tf.truncated_normal([num_input, n_hidden]), name="weights")
    biases = tf.Variable(tf.zeros([n_hidden]), name="biases")
    z = tf.matmul(xIn, weights) + biases
    hidden = tf.nn.relu(z)
    return hidden



features = tf.placeholder(tf.float32, [None, 784])
labels = tf.placeholder(tf.float32, [None, 10])
logits_train = ann(features, num_classes)
# Predictions
pred_classes = tf.argmax(logits_train, axis=1)




    # Define loss and optimizer
loss_op = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(
    logits=logits_train, labels=tf.cast(labels, dtype=tf.int32)))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
train_op = optimizer.minimize(loss_op, global_step=tf.train.get_global_step())



correct_prediction = tf.equal(tf.argmax(logits_train, 1), tf.argmax(labels, 1)) 
acc_op = tf.reduce_mean(tf.cast(correct_prediction, tf.float32)) 
# TF Estimators requires to return a EstimatorSpec, that specify
# the different ops for training, evaluating, ...
with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    # Build the Estimator
    for i in range(50):
        for j in range(batch_size):
            batch = mnist.train.next_batch(batch_size) 
            _, accuracy =sess.run([train_op, acc_op], feed_dict = {features: batch[0], labels:batch[1]} )
        print (accuracy)    

# Evaluate the Model
